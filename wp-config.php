<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'myShop');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'james');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',-z+H!IXK9D aPJW1Lx+z#YCj_i.zZRl;7gP0hzf!C8XY-Cr~H!!s-3W4kqp]t>`');
define('SECURE_AUTH_KEY',  'l6z+4IB=(@fB}xhs1EG.5Pi_x<y^Q~Cd}F;[QWHR|V9;^c74vzTWp=XwQ}L-m>QQ');
define('LOGGED_IN_KEY',    '4kt}[Jr1HE/Bt-|xo<1M pwVFuS43M`+KmbW rdU(;*W0!c@Tw<r5G2s6!@gAX//');
define('NONCE_KEY',        'gx1^8?fR<SpFN PVz+O>ey&&2Z-k.H-qb+sUv6G;P2+1QT`.RKPc]Avetw#a`_Y$');
define('AUTH_SALT',        '2PT|J+1,H]hvD=[n+]N/(-T(g:+,B-$|RCdwQS)Ojm-j+&4^ YH@W} M)_-HChg%');
define('SECURE_AUTH_SALT', ',-L$TWbz&uDDJXy8djVZu{B+<_v;+W?.P@/T^6;I6z={<;s}y%qZU|)4(<:7Ao%X');
define('LOGGED_IN_SALT',   '(U&pp#P.*m1hV-%!w (R4Xu3.155+Bk?J5JlaBhw?=tmanuq<p77Z}Z+{w,?H46~');
define('NONCE_SALT',       'Du>p$StH2+~)FnhA_2hC|z^l{}g*x6>|=;^{*1G+27ZOYruJRJI3Y_b4]h%&zRNh');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
