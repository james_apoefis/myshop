module.exports = function(grunt) {
	grunt.initConfig({
		concat: {
			dist: {
				src: ['js/global-functions.js', 'js/main.js', 'js/header.js'],
				dest: 'scripts.js'
			}
		},
		jshint: {
			beforeconcat: ['js/**/*.js'],
			afterconcat: ['scripts.js']
		},
		uglify: {
			options: {
				mangle: true
			},
			my_target: {
				files: {
					'scripts.min.js': ['scripts.js']
				}
			}
		},
		sass: {
			options: {
				sourceMap: true
			},
			dist: {
				files: {
					'style.css': 'css/scss/style.scss'
				}
			}
		},
		autoprefixer: {
			options: {
				browsers: ['> 15%', 'last 5 versions']
			},
			single_file: {
				src: 'style.css',
				dest: 'style.css'
			}
		},
		watch: {
			scripts: {
				files: 'js/**/*.js',
				tasks: ['jshint', 'concat', 'uglify']
			},
			css: {
				files: 'css/scss/**/*.scss',
				tasks: ['sass', 'autoprefixer']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');

    // Run using `grunt` this will watch all changes but not minify
    grunt.registerTask('default', ['jshint', 'concat', 'sass', 'watch']);

    // Run using `grunt prod`, this will compile and minify but not watch
    grunt.registerTask('prod', ['jshint', 'concat', 'uglify', 'sass', 'autoprefixer']);

};