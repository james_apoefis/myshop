<div class="subscribe-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <p class="subscribe-title">SUBSCRIBE TO OUR LATEST OFFERS</p>
                    <p class="subscribe-sub">Be the first to recieve the latest news and offers (with our no spam policy)</p>
                    <div class="subscribe-form">
                        <input type="text" placeholder="Enter your email">
                        <button type="submit" id="submit" title="search"></button>
                    </div>
                    <p class="subscribe-copy">You are signing up to receive product updates and newsletters. By signing up, you are consenting to our <a href="#">privacy policy</a> but you can opt out at any time.</p>

                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="contact-info">
                            <h2>Contact Information</h2>
                            <p><span>P</span>02 4444 4444</p>
                            <p><span>E</span>info@myshop.com</p>
                            <p id="address">23 Wright Street<br> Queanbeyan NSW 2620</p>
                        </div>
                        <div class="socials">
                            <h2>Follow Us</h2>
                            <ul>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon/facebook.svg);" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon/facebook.svg);" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon/facebook.svg);" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon/facebook.svg);" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon/facebook.svg);" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h2>Frequently Asked Questions</h2>
                        <div class="faq-wrapper">
                            <ul>
                                <li><a href="#">Lorem ipsum dolor sit amet requiem lorem</a></li>
                                <li><a href="#">Lorem ipsum dolor sit amet requiem lorem</a></li>
                                <li><a href="#">Lorem ipsum dolor sit amet requiem lorem</a></li>
                                <li><a href="#">Lorem ipsum dolor sit amet requiem lorem</a></li>
                                <li><a href="#">Lorem ipsum dolor sit amet requiem lorem</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h2>Twitter Feed</h2>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p id="copyright">© 2018 My Store</p>
                        <p id="website-by">Website By: <a href="#">James Apoefis</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>