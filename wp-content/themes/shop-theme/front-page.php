<?php get_header(); ?>

    <?php get_template_part("template-parts/homepage/feature-banner"); ?>
    <?php get_template_part("template-parts/homepage/about-us"); ?>
    <?php get_template_part("template-parts/homepage/featured-products"); ?>
    <?php get_template_part("template-parts/homepage/cta"); ?>
    <?php get_template_part("template-parts/homepage/testimonials"); ?>

<?php get_footer(); ?>    