<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:700" rel="stylesheet">

    <?php wp_head(); ?>
</head>
<body>
    <div class="header-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <header>
                        <img id="logo" src="<?php echo get_template_directory_uri() ?>/img/icon/my-shop-logo.svg" alt="My Shop Logo">
                        <nav class="menu-main-menu">
                            <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
                        </nav>
                        <div class="shop-toolbar">
                            <ul>
                                <li><img src="<?php echo get_template_directory_uri() ?>/img/icon/location.svg" alt="Find a My Shop store near you"></li>
                                <li><img src="<?php echo get_template_directory_uri() ?>/img/icon/cart.svg" alt="View your cart"></li>   
                                <li><img src="<?php echo get_template_directory_uri() ?>/img/icon/search.svg" alt="Search My Shop"></li>                           
                            </ul>
                        </div>
                    </header>
                </div>
            </div>
        </div>
    </div>