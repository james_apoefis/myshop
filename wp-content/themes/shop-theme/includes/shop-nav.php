<?php

    // Register Menu's

    function registerMenus() {
        register_nav_menus(
            array(
              'main-menu' => __( 'Header Menu' )
            )
        );
    }

    add_action( 'init', 'registerMenus' );