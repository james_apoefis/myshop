<?php

function mp6_override_toolbar_margin() {
	if ( is_admin_bar_showing() ) {
		add_filter('show_admin_bar', '__return_false');?>
		<style type="text/css" media="screen">
			html { margin-top: 0 !important; }
			* html body { margin-top: 0 !important; }
		</style>
	<?php }
} add_action( 'wp_head', 'mp6_override_toolbar_margin', 11 );

add_theme_support('menus');