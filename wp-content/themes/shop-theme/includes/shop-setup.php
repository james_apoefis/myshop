<?php

    /* Enqueue Scripts */

    function enqueue_shop_scripts() {
        wp_enqueue_script( 'main', get_template_directory_uri() . '/scripts.js', array('jquery'), '', true );
    }

    add_action('wp_enqueue_scripts', 'enqueue_shop_scripts');

    /* Enqueue Scripts */

    function enqueue_shop_styles() {
        wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css');
    }

    add_action('wp_enqueue_scripts', 'enqueue_shop_styles');