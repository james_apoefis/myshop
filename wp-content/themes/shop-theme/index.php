<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <?php wp_head(); ?>
</head>
<body>
    <h1>Main</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Neque ad provident molestias id sequi dignissimos dolore non totam, dolor omnis! Ad et corrupti nostrum a, hic labore vero at iure!</p>
            </div>
            <div class="col-md-6">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Neque ad provident molestias id sequi dignissimos dolore non totam, dolor omnis! Ad et corrupti nostrum a, hic labore vero at iure!</p>
            </div>
        </div>
    </div>
</body>
</html>