<?php get_header(); ?>

<?php get_template_part("template-parts/content/page-title"); ?>

<section class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>This is a page title</h2>

                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique modi, dignissimos consequuntur vel laboriosam, pariatur fugit delectus doloribus harum minus dolorem veritatis ipsa quasi sed, perspiciatis impedit neque maxime qui? Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quidem ipsam dolorum unde ducimus, quod architecto. Aspernatur possimus quae ratione, eligendi itaque ex labore laudantium, iste fugit sunt iusto alias. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est libero nostrum cumque autem officia suscipit. Velit consectetur dolorem, alias minima dolor quibusdam obcaecati ea a dolores ad similique iusto expedita!</p>

                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique modi, dignissimos consequuntur vel laboriosam, pariatur fugit delectus doloribus harum minus dolorem veritatis ipsa quasi sed, perspiciatis impedit neque maxime qui? Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quidem ipsam dolorum unde ducimus, quod architecto. Aspernatur possimus quae ratione, eligendi itaque ex labore laudantium, iste fugit sunt iusto alias. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est libero nostrum cumque autem officia suscipit. Velit consectetur dolorem, alias minima dolor quibusdam obcaecati ea a dolores ad similique iusto expedita!</p>
            </div>
            <div class="col-md-4">
                <h2>Categories</h2>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>