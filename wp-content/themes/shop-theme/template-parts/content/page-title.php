<div class="page-title" style="background-image: url(<?php echo get_template_directory_uri() ?>/img/feature_home.jpg);">
    <div class="layer">
        <div class="content">
            <h1 class="feature-title">Home page with a really long title</h1>
            <p> 18 Oct 2018 <span> | </span> By James Apoefis <span> | </span> <a href="#">Cat one</a>, <a href="#"> Cat two</a> </p>
        </div>
    </div>
</div>