<div class="about-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>About My Store</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis porttitor porta sit amet in mauris. Aliquam sed cursus nunc. Vestibulum non nulla eros. amet in mauris. Aliquam sed cursus nunc. Vestibulum non nulla eros ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue.</p>
            </div>
        </div>
    </div>
</div>

<div class="about-feature" style="background-image: url(<?php echo get_template_directory_uri() ?>/img/dark_desk2.jpg);">
    <div class="layer">
        <div class="about-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <h2>100% Trusted To Deliver</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis porttitor porta sit amet in mauris. Aliquam sed cursus nunc. Vestibulum non nulla eros.</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="tile-container">
                        <div class="col-md-4">
                            <div class="tile">
                                <img src="<?php echo get_template_directory_uri() ?>/img/icon/heart.svg);" alt="">
                                <h3>Trusted by 10,000 Customers</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tile">
                                <img src="<?php echo get_template_directory_uri() ?>/img/icon/truck.svg);" alt="">
                                <h3>Fast Shipping</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tile">
                                <img src="<?php echo get_template_directory_uri() ?>/img/icon/desktop.svg);" alt="">
                                <h3>Track Your Shipment</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>