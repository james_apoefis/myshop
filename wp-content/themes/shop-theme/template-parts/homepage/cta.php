<div class="cta-wrapper" style="background-image: url(<?php echo get_template_directory_uri() ?>/img/apple_computer.jpg);">
    <div class="layer">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h2>View Our Entire Range Of Products</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis porttitor porta sit amet in mauris. Aliquam sed cursus nunc. Vestibulum non nulla.</p>
                    <a class="btn btn-white" href="#">SHOP NOW</a>
                </div>
            </div>
        </div>
    </div>
</div>