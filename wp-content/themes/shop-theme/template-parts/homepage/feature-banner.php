<div class="feature-banner" style="background-image: url(<?php echo get_template_directory_uri() ?>/img/feature_home.jpg);">
    <div class="layer">
        <div class="content">
            <h1 class="feature-title">Quality Products To Increase Your Productivity</h1>
            <a class="btn btn-white" href="#">Shop Now</a>
        </div>
    </div>
</div>