<div class="featured-products">
    <div class="container">
        <h2>Featured Products</h2>
        <?php echo do_shortcode("[featured_products limit='9' columns='3']"); ?>
        </div>
    </div>
</div>