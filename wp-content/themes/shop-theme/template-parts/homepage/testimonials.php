<div class="testimonial-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h2>Testimonials</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis porttitor porta sit amet in mauris.</p>
            </div>
        </div>
        <div class="row">
            <div class="testimonials">
                <div class="col-md-4">
                    <img src="<?php echo get_template_directory_uri() ?>/img/testimonial_one.jpg);" alt="">
                    <h3>James Apoefis</h3>
                    <cite>Facebook Review</cite>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis porttitor porta sit amet in mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue.</p>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo get_template_directory_uri() ?>/img/testimonial_one.jpg);" alt="">
                    <h3>James Apoefis</h3>
                    <cite>Facebook Review</cite>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis porttitor porta sit amet in mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue.</p>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo get_template_directory_uri() ?>/img/testimonial_one.jpg);" alt="">
                    <h3>James Apoefis</h3>
                    <cite>Facebook Review</cite>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue. Donec in tellus quis ante finibus rhoncus. Vivamus aliquam orci sed turpis porttitor porta sit amet in mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id velit augue.</p>
                </div>
            </div>
        </div>
    </div>
</div>